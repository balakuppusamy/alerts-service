package com.alerts.utils;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.NotNullPredicate;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public class AlertUtils {

    public static String getSystemValue(String systemKey) {
        String envUrl = StringUtils.trimToNull(System.getenv(systemKey));
        String propUrl = StringUtils.trimToNull(System.getProperty(systemKey));
        return (String) CollectionUtils
                .find(Arrays.asList(envUrl, propUrl, StringUtils.EMPTY), NotNullPredicate.INSTANCE);
    }
}