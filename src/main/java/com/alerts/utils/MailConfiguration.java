package com.alerts.utils;

public enum MailConfiguration {
    MAIL_HOST,
    MAIL_FROM_ADDRESS,
    MAIL_TO_ADDRESS,
}