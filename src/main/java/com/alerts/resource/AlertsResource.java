package com.alerts.resource;

import com.alerts.model.EmailBean;
import com.alerts.services.EmailService;
import com.alerts.utils.AlertUtils;
import com.alerts.utils.MailConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("alerts")
public class AlertsResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlertsResource.class);

    @Inject
    EmailService emailService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response sendAlert(EmailBean emailBean) throws WebApplicationException {
        LOGGER.info("sendAlert starts emailBean:{}", emailBean);
        try {
            if(StringUtils.isBlank(emailBean.getFrom())){
                emailBean.setFrom(AlertUtils.getSystemValue(MailConfiguration.MAIL_FROM_ADDRESS.name()));
            }
            if(StringUtils.isBlank(emailBean.getTo())){
                emailBean.setTo(AlertUtils.getSystemValue(MailConfiguration.MAIL_TO_ADDRESS.name()));
            }
            emailService.sendAlertMail(emailBean);

        }
        catch (EmailException ex) {
            LOGGER.error("Sending alert was failed", ex);
            throw new WebApplicationException("Sending alert was failed", ex);
        }
        LOGGER.info("sendAlert ends");
        return Response.status(Response.Status.OK).entity("Alert has been sent").build();
    }
}