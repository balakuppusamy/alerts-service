package com.alerts;

import com.alerts.services.EmailService;
import com.alerts.services.EmailServiceImpl;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import javax.inject.Singleton;

public class DependencyBinder extends ResourceConfig {
    public DependencyBinder() {
        register(JacksonFeature.class);
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(EmailServiceImpl.class).to(EmailService.class).in(Singleton.class);
            }
        });
    }
}