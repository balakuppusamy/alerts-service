package com.alerts.services;

import com.alerts.model.EmailBean;
import com.alerts.utils.AlertUtils;
import com.alerts.utils.MailConfiguration;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.glassfish.hk2.api.PostConstruct;
import org.jvnet.hk2.annotations.Service;

@Service
public class EmailServiceImpl implements EmailService, PostConstruct {

    private String hostName;

    @Override
    public void postConstruct() {
        hostName = AlertUtils.getSystemValue(MailConfiguration.MAIL_HOST.name());
    }

    @Override
    public void sendAlertMail(EmailBean emailBean) throws EmailException {
        Email email = new SimpleEmail();
        email.setHostName(hostName);
        email.setFrom(emailBean.getFrom());
        email.setSubject(emailBean.getTo());
        email.setMsg(emailBean.getMessage());
        email.addTo(emailBean.getTo());
        email.send();
    }
}