package com.alerts.services;

import com.alerts.model.EmailBean;
import org.apache.commons.mail.EmailException;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface EmailService {
   void sendAlertMail(EmailBean emailBean) throws EmailException;
}