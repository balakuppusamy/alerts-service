package com.alerts.services;

import com.alerts.model.EmailBean;
import com.alerts.utils.MailConfiguration;
import org.apache.commons.mail.EmailException;
import org.junit.Before;
import org.junit.Test;

public class EmailServiceImplTest {

    @Before
    public void setUp(){
        System.setProperty(MailConfiguration.MAIL_HOST.name(), "icct-com.mail.protection.outlook.com");
    }
    @Test
    public void testSendAlertMail() throws EmailException {
        EmailServiceImpl emailService = new EmailServiceImpl();
        emailService.postConstruct();
        EmailBean emailBean = new EmailBean();
        emailBean.setMessage("Test");
        emailBean.setSubject("Test");
        emailBean.setFrom("bkuppusamy@icct.com");
        emailBean.setTo("bkuppusamy@icct.com");
        emailService.sendAlertMail(emailBean);
    }
}