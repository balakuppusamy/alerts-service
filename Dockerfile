FROM jetty:latest

USER root

RUN apt-get update

ADD target/alert-service*.war /tmp/alert-service.war
ENV MAIL_HOST=icct-com.mail.protection.outlook.com \
    MAIL_FROM_ADDRESS=bkuppusamy@icct.com \
    MAIL_TO_ADDRESS=bkuppusamy@icct.com

RUN mv /tmp/alert-service.war /var/lib/jetty/webapps/root.war
RUN chmod -R 755 /var/lib/jetty/webapps/root.war

EXPOSE 8080